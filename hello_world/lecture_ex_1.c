//
//  lecture_ex_1.c
//  hello_world
//
//  Created by Suganithiselvan on 22/10/18.
//  Copyright © 2018 Suganithiselvan. All rights reserved.
//

#include "lecture_ex_1.h"
#include <stdio.h>

int main(){
    float float1, float2, quotation;
    
    printf("Enter two float values: ");
    scanf("%f%f", &float1, &float2);
    
    quotation = float1/float2;
    
    printf("Your result is: %f", quotation);
    
    return 0;
}
