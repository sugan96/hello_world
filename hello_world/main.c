//
//  main.c
//  hello_world
//
//  Created by Suganithiselvan on 22/10/18.
//  Copyright © 2018 Suganithiselvan. All rights reserved.
//

#include <stdio.h>

int main() {
    float float1, float2, quotation;
    
    printf("Enter two float values:\n");
    scanf("%f%f", &float1, &float2); //input 2 values
    
    quotation = float1/float2; //devide them
    
    printf("Your result is: %f", quotation); //print results
    return 0;
}
